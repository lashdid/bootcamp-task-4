import { useState } from "react";
import { Homepage } from "./pages/Homepage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Layout } from "./components/Layout"
import { View } from "./pages/View";
import { About } from "./pages/About";
import { Hobby } from "./pages/Hobby";

export default function App() {
  const [data, setData] = useState([]);

  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Homepage data={data} setData={setData}/>
        </Route>
        <Route path='/view/:id'>
          <View data={data}/>
        </Route>
        <Layout>
          <Route exact path='/about'>
            <About/>
          </Route>
          <Route exact path='/hobby'>
            <Hobby data={data}/>
          </Route>
        </Layout>
      </Switch>
    </Router>
  );
}
