import { CustomAppBar } from "../components/CustomAppBar";
import { Content } from "../components/Content";
import { CustomModal } from "../components/CustomModal";
import { useState } from "react";
import { ListContent } from "../components/ListContent";

export const Homepage = ({data, setData}) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [dataId, setDataId] = useState();

  const onSave = (item) => {
    if (item.id === dataId) {
      const newData = data.map((oldData) => {
        if (oldData.id === item.id) {
          return item;
        }
        return oldData;
      });
      setData(newData);
      setModalOpen(false);
      setDataId();
    } else {
      setData([...data, item]);
      setModalOpen(false);
    }
  };

  return (
    <main>
      <CustomAppBar onButtonClick={() => setModalOpen(true)} />
      {data.length === 0 ? (
        <Content />
      ) : (
        <ListContent
          listArray={data}
          onEdit={(id) => {
            setModalOpen(true);
            setDataId(id);
          }}
        />
      )}
      <CustomModal
        isModalOpen={isModalOpen}
        onModalClose={() => {
          setModalOpen(false);
          setDataId();
        }}
        editValue={dataId && data.find((item) => item.id === dataId)}
        onFormSave={onSave}
      />
    </main>
  );
}
