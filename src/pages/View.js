import { Box, Card } from "@mui/material";
import Typography from "@mui/material/Typography";
import { useParams } from "react-router-dom";
import { BackAppBar } from "../components/BackAppBar";

export const View = ({ data }) => {
  const { id } = useParams();
  const viewData = data.find((item) => item.id === parseInt(id));
  return (
    <main>
      <BackAppBar/>
      <Box style={{display: 'flex', flexDirection: 'column', gap: '20px', padding: '20px'}}>
        <Card>
          <Typography variant="body2" color="text.secondary">
            Name
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {viewData.name}
          </Typography>
        </Card>
        <Card>
          <Typography variant="body2" color="text.secondary">
            Address
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {viewData.address}
          </Typography>
        </Card>
        <Card>
          <Typography variant="body2" color="text.secondary">
            Hobby
          </Typography>
          <Typography gutterBottom variant="h5" component="div">
            {viewData.hobby}
          </Typography>
        </Card>
      </Box>
    </main>
  );
}
