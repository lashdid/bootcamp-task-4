import { Box, Card, Typography } from "@mui/material";
import React from "react";

export const About = () => {
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "20px",
        padding: "20px",
      }}
    >
      <Card style={{ padding: "20px" }}>
        <Typography gutterBottom variant="h5" component="div">
          About Us
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem,
          inventore. Similique laboriosam fugit eaque qui delectus. Blanditiis,
          atque minima! Laudantium, provident ex eos aliquam deleniti eligendi
          perferendis, ullam vitae asperiores odit ipsum quos. Inventore
          nesciunt nostrum sequi doloribus incidunt reprehenderit vitae? Autem
          quasi cum voluptates nesciunt quae asperiores eum, corrupti totam
          quaerat, veniam fuga, libero doloribus consequatur? Consequatur,
          aliquam deleniti totam maxime perspiciatis necessitatibus cumque
          impedit reiciendis accusantium esse deserunt reprehenderit autem quod
          provident labore, animi voluptatibus illum incidunt fugit et iusto
          tenetur ex delectus alias. Dolores error veritatis, delectus
          cupiditate animi dolore, nemo, numquam commodi saepe ipsa dolorum quam
          exercitationem eligendi minima odio ex facere magni tempora magnam
          incidunt enim eum. Saepe dolorem beatae optio dicta provident,
          doloremque sapiente delectus atque. Impedit eum, veritatis saepe
          maxime quae mollitia accusantium est suscipit pariatur quisquam qui
          nisi iusto molestiae repudiandae fugiat asperiores dolore sed vero
          consequatur blanditiis rem! Asperiores, architecto perspiciatis! Fugit
          eos neque rem, quam dolor molestias iure corporis nesciunt soluta
          officiis. Perferendis, consequuntur alias? Vel, qui! Magnam, corrupti
          architecto? Accusantium magnam quidem doloremque! Ducimus quos qui,
          est illo ut iusto voluptatum blanditiis, possimus atque vel,
          repellendus eos commodi. Placeat cumque earum reiciendis aliquam,
          quisquam dolorem ex nobis minus odio cupiditate quasi dolor
          repellendus consequuntur, aliquid repudiandae ab eveniet. Unde omnis
          quisquam vel natus consectetur ex nostrum laudantium rerum laborum
          tempora, dolore reprehenderit asperiores nam dolorum voluptatum
          obcaecati nobis aut at ab praesentium iste corporis ducimus totam
          reiciendis? Perferendis laborum nemo fuga adipisci similique
          dignissimos, ex saepe unde ipsum vitae quod odit, quo enim officia
          distinctio nulla aspernatur dolorum eligendi est maxime et sed
          necessitatibus mollitia! Veritatis repudiandae dolore quisquam
          pariatur, laudantium voluptate reiciendis ab illo dolorum doloribus
          totam expedita debitis. Fugiat maiores ad blanditiis quod nesciunt
          similique pariatur, officia impedit natus quisquam non repellat eius
          ex harum minima perferendis totam aut doloremque quae mollitia
          sapiente placeat. Enim blanditiis iure ullam adipisci quisquam.
          Tenetur suscipit consectetur, omnis voluptate asperiores officia!
          Illum explicabo eum veniam eos nisi consequuntur aliquam cum sed ad
          ipsum. Unde fugit eligendi impedit ipsum cumque. Nesciunt explicabo
          rem, autem voluptatum ipsam animi tempora cum optio, deleniti ratione
          saepe hic? Labore quod officia ut debitis impedit cupiditate eum,
          culpa commodi reprehenderit harum, tenetur libero ducimus quam.
          Delectus possimus error quod accusamus dolor quis suscipit ab debitis
          reiciendis, aspernatur vel et qui explicabo eum laboriosam minima
          mollitia eos veniam officia repellendus. Ad nemo aperiam eligendi esse
          illum possimus voluptate corporis eaque. Veniam, laudantium aperiam
          ipsum corrupti nisi est id quia nobis quo provident minus ut animi
          asperiores eligendi non qui. Ab odit cupiditate, illo labore
          temporibus laudantium ipsam repellendus quasi non maiores, aliquam
          deleniti. Omnis architecto esse laborum ullam cum repellendus sed qui
          pariatur sapiente nobis eveniet neque unde corrupti ducimus atque
          tenetur consequuntur quaerat quisquam quidem, officia iusto numquam
          ex. Ea amet dignissimos assumenda minus iusto quos nostrum. Aperiam
          recusandae optio, error sed nemo, perspiciatis deserunt delectus
          voluptas praesentium quaerat minima. Delectus, ipsum. Dolore sunt
          officia, pariatur asperiores, ad minus saepe totam eum nostrum culpa,
          amet iure dicta.
        </Typography>
      </Card>
    </Box>
  );
};
