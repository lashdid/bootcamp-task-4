import { Box, Card, Typography } from "@mui/material";
import React from "react";

export const Hobby = ({ data }) => {
  const hobbies = data && data.map((item) => item.hobby);
  const uniqueHobbies = [...new Set(hobbies)];
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "20px",
        padding: "20px",
      }}
    >
      <Typography gutterBottom variant="h5" component="div">
        List of Hobbies
      </Typography>
      <Typography color="text.secondary" variant="span" component="div">
        A list of user hobbies without duplicate
      </Typography>
      {data.length === 0 ? (
        <Typography variant="body2" color="text.secondary">
          Data is empty
        </Typography>
      ) : (
        uniqueHobbies.map((hobbies) => (
          <Card key={hobbies}>
            <Typography gutterBottom variant="h5" component="div">
              - {hobbies}
            </Typography>
          </Card>
        ))
      )}
    </Box>
  );
};
