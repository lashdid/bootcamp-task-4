import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { NavLink } from "react-router-dom";

export const CustomAppBar = ({ onButtonClick, noButton }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ backgroundColor: "#004aad" }}>
        <Toolbar style={{ display: "flex", justifyContent: "space-between" }}>
          <div style={{ display: "flex", gap: "20px" }}>
            <Typography
              fontWeight={600}
              variant="h6"
              component="div"
              sx={{ flexGrow: 1 }}
            >
              My App
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <NavLink
                exact
                to="/"
                style={{ color: "white", textDecoration: "none" }}
                activeStyle={{textDecoration: 'underline'}}
              >
                Home
              </NavLink>
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <NavLink
                to="/about"
                style={{ color: "white", textDecoration: "none" }}
                activeStyle={{textDecoration: 'underline'}}
              >
                About
              </NavLink>
            </Typography>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <NavLink
                to="/hobby"
                style={{ color: "white", textDecoration: "none" }}
                activeStyle={{textDecoration: 'underline'}}
              >
                Hobby
              </NavLink>
            </Typography>
          </div>
          {!noButton && (
            <Button
              onClick={onButtonClick}
              variant="contained"
              style={{
                backgroundColor: "#00c2cb",
                fontSize: "18px",
                textTransform: "none",
                borderRadius: "50px",
              }}
            >
              Add User
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};
