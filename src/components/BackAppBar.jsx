import { ArrowBack } from "@mui/icons-material";
import { AppBar, Box, Toolbar, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

export const BackAppBar = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ backgroundColor: "#004aad" }}>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link
              to="/"
              style={{
                display: "flex",
                alignItems: "center",
                color: "white",
                gap: "1rem",
                textDecoration: "none",
              }}
            >
              <ArrowBack /> <span style={{ marginTop: "5px" }}>Back</span>
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
