import { CustomAppBar } from "./CustomAppBar";

export const Layout = ({children}) => {
  return (
    <main>
      <CustomAppBar noButton={true}/>
      {children}
    </main>
  );
};
