import { Container, Typography } from "@mui/material";
import React from "react";
import { Circle } from "./Circle";

export const Content = () => {
  return (
    <Container
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "90vh",
      }}
    >
      <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
        <Circle/>
        <Typography variant="h1" style={{color: '#ff5757', fontWeight: 'bold', fontFamily: "'Times New Roman', Times, serif"}}><i>USER</i></Typography>
      </div>
    </Container>
  );
};
